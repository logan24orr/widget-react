import dotenv from 'dotenv';
dotenv.config();

import {fetchQuestions, submitForm} from '../src/api';

test('test fetch questions', async () => {
  const questions = await fetchQuestions();
  expect(questions.questions.length).toEqual(7);
});

test('test submit form', async () => {
  const response = await submitForm('John', 'Doe', '000-000-0000', 'john@demo.com', [
    {
      id: '797a8a7e-a34c-4bba-9f88-08eea6580e6d',
      answer: 'yes',
      question: 'Were you injured in an accident?',
      question_type: 'true_false',
    },
    {
      id: '505a67b2-b728-46ec-903f-71999088e574',
      answer: 'no',
      question: 'Were you at fault in the accident?',
      question_type: 'true_false',
    },
    {
      id: '505a67b2-b728-46ec-903f-710b9088e574',
      answer: 'yes',
      question: 'Do you or the fault party have insurance?',
      question_type: 'true_false',
    },
    {
      id: '0d9b3cb6-c73a-4f63-9511-c2ceb9c560d3',
      answer: 'yes',
      question: 'Was the accident in the last 2 years?',
      question_type: 'true_false',
    },
    {
      id: '505a67b2-b728-46ec-903f-710b9088e5835',
      answer: 'Utah',
      question: 'What state did the accident happen in?',
      question_type: 'selection',
    },
    {
      id: '94267db6-5139-4f39-aae1-a3a5f4613363',
      answer: 'Broken back',
      question: 'Please describe your injuries from the accident.',
      question_type: 'text_input',
    },
  ]);
  expect(response).toEqual(true);
});
