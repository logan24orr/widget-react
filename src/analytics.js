/* eslint-disable require-jsdoc */
import {v4 as uuidv4} from 'uuid';
import {sendAnalytic} from './api.js';

export async function initUniqueVisitor() {
  if (!localStorage.getItem('uniqueVisitorId')) {
    const uniqueVisitorId = uuidv4();
    localStorage.setItem('uniqueVisitorId', uniqueVisitorId);
  }
}

export function trackUniqueSiteVisitor() {
  const event = 'sv';
  sendAnalytic(event);
}

export function trackUniqueCTAButtonClick() {
  const event = 'bc';
  sendAnalytic(event);
}

export function trackUniqueFormSubmission() {
  const event = 'fs';
  sendAnalytic(event);
}
