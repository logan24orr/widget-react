/* eslint-disable require-jsdoc */
import React, {useState, useEffect} from 'react';
import Modal from './components/modal.js';
import BannerFloating from './components/banner.js';

export default function App() {
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
  }, []);

  const toggleModal = () => {
    setShowModal(!showModal);
  };

  return (
    <div className="z-[2147482998]">
      <BannerFloating onRegisterClick={toggleModal} />
      {showModal && <Modal onClose={toggleModal} />}
    </div>
  );
}


