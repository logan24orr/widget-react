/* eslint-disable require-jsdoc */
import {createClient} from '@supabase/supabase-js';

const supabase = createClient(
    'https://ggtrccwlznsnjnpamsly.supabase.co',
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImdndHJjY3dsem5zbmpucGFtc2x5Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDgxOTcxMjEsImV4cCI6MjAyMzc3MzEyMX0.Q-PWQGtjLW4OPJTu0DsUtEeByyDJUtpYsMkZiBR50m8',
);

export async function fetchQuestions() {
  const websiteDomain = process.env.WEBSITE_DOMAIN;

  try {
    const {data, error} = await supabase
        .from('question')
        .select('*')
        .eq('website', websiteDomain)
        .single();

    if (error) {
      throw new Error(error);
    }

    return data.questions;
  } catch (error) {
    console.error(error);
  }
  return [];
}

export async function submitForm(fullName, phone, email, answers) {
  const websiteDomain = process.env.WEBSITE_DOMAIN;
  try {
    await supabase
        .from('submission')
        .insert([
        // eslint-disable-next-line object-shorthand
          {website: websiteDomain, full_name: fullName, phone: phone, email: email, answers: answers},
        ]);
  } catch (error) {
    console.error(error);
  }
  return true;
}

// export async function sendAnalytic(event) {
//   let visitorId;

//   if (typeof localStorage !== 'undefined') {
//     visitorId = localStorage.getItem('uniqueVisitorId');
//   } else {
//     visitorId = process.env.VISITOR_ID;
//   }
//   const websiteDomain = process.env.WEBSITE_DOMAIN;

//   if (visitorId) {
//     try {
//       const {error} = await supabase
//           .from('analytics-data')
//           .insert([
//             {visitor_id: visitorId, source: websiteDomain, event_name: event},
//           ]);

//       if (error) {
//         throw new Error(error);
//       }
//     } catch (error) {
//       console.error(error);
//     }
//     return true;
//   }
// }
