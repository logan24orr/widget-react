/* eslint-disable require-jsdoc */
/* eslint-disable max-len */
import React, {Fragment, useState} from 'react';
import {Dialog, Transition} from '@headlessui/react';
import PropTypes from 'prop-types';
import Form from './form_components/form.js';

export default function Modal({onClose}) {
  const [open, setOpen] = useState(true);

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog as="div" className="relative z-[99999999999]" onClose={setOpen}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
        </Transition.Child>

        <div className="h-full fixed inset-0 z-10 w-screen z-[99999]">
          <div className="flex min-h-full items-end justify-center text-center sm:items-center sm:p-0">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
              enterTo="opacity-100 translate-y-0 sm:scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 translate-y-0 sm:scale-100"
              leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            >
              <Dialog.Panel className="h-full relative transform overflow-hidden md:rounded-lg bg-white text-left shadow-xl transition-all sm:my-0 sm:w-full sm:max-w-4xl">
                <div className="w-full h-screen md:h-full md:min-h-[750px] bg-white">
                  <Form onClose={onClose} />
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition.Root>
  );
}

Modal.propTypes = {
  onClose: PropTypes.func,
};
