/* eslint-disable require-jsdoc */
import React, {useState} from 'react';
import '../styles.css';
import {XMarkIcon} from '@heroicons/react/20/solid';
import cruzLogo from '../assets/cruz_logo_square_2.png';
import PropTypes from 'prop-types';

export default function BannerFloating({onRegisterClick}) {
  const [visible, setVisible] = useState(true);

  return (
    <div className="fixed bottom-0 left-0 z-[2147482998]">
      {visible &&
              <div>
                <div className="hidden fixed pointer-events-none fixed inset-x-0 bottom-0 lg:flex lg:justify-center lg:px-6 lg:pb-5 lg:px-8">
                  <div className="pointer-events-auto flex items-center justify-between gap-x-6 bg-black px-6 py-2.5 sm:rounded-xl sm:py-3 sm:pl-4 sm:pr-3.5">
                    <span className="text-lg leading-6 text-white">
                      <a href="#" className="pr-4">
                        <div className="inline pr-4">
                          <img src={cruzLogo} alt="cruz logo" className="h-12 w-auto inline" />
                        </div>
                        <strong className="font-semibold">Need Cash?</strong>
                        <svg viewBox="0 0 2 2" className="mx-2 inline h-0.5 w-0.5 fill-current" aria-hidden="true">
                          <circle cx={1} cy={1} r={1} />
                        </svg>
                                  See if You Qualify for a Cash Advance from a 3rd Party Financial Institution
                      </a>
                      <button
                        type="button"
                        className="pl-4 rounded-full bg-[#2392c2] px-3 py-1.5 text-sm font-semibold text-white shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-bannerButton"
                        onClick={onRegisterClick}
                      >
                        <span className="text-white text-lg">Get $500<span aria-hidden="true" className="ml-1">&rarr;</span></span>
                      </button>
                    </span>
                    <button type="button" className="-m-1.5 flex-none p-1.5">
                      <span className="sr-only">Dismiss</span>
                      <XMarkIcon className="h-5 w-5 text-white" aria-hidden="true" onClick={() => setVisible(false)} />
                    </button>
                  </div>
                </div>
                <div className="lg:hidden fixed pointer-events-none fixed inset-x-0 bottom-0 sm:flex sm:justify-center sm:px-6 sm:pb-5 lg:px-8">
                  <div className="h-24 pointer-events-auto flex items-center justify-between gap-x-6 bg-black sm:px-6 py-2.5 sm:rounded-xl sm:py-3 sm:pl-4 sm:pr-3.5">
                    <div className="w-full flex justify-between items-center">
                      <div className="flex items-center">
                        <button type="button" className="flex-none">
                          <span className="sr-only">Dismiss</span>
                          <XMarkIcon className="h-5 w-5 text-white mx-2" aria-hidden="true" onClick={() => setVisible(false)} />
                        </button>
                        {/* <img src={cruzLogo} alt="cruz logo" className="h-12 w-auto inline" /> */}
                        <span className="text-white w-full text-sm sm:text-lg">See if You Qualify for a Cash Advance from a 3rd Party Financial Institution</span>
                        <button
                          type="button"
                          className="min-w-24 sm:min-w-36 pl-1 mx-2 rounded-full bg-[#2392c2] p2-3 py-1.5 text-sm sm:text-lg font-semibold text-white shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2"
                          onClick={onRegisterClick}
                        >
                          <span className="text-white">Get $500<span aria-hidden="true" className="ml-1">&rarr;</span></span>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
      }
    </div>
  );
}

BannerFloating.propTypes = {
  onRegisterClick: PropTypes.func,
};
