/* eslint-disable require-jsdoc */
import React, {useEffect, useState} from 'react';
import {Transition} from '@headlessui/react';
import FadeIn from './fade_in.js';
import PropTypes from 'prop-types';

export default function TextInputQuestion({question, addAnswer, removeAnswer, answers}) {
  const [show, setShow] = useState(false);
  const [text, setText] = useState('');

  useEffect(() => {
    setShow(true);
    const indexAnswer = answers.findIndex((answer) => answer.id === question.id);
    if (indexAnswer !== -1) {
      setText(answers[indexAnswer].answer);
    }
  }, []);

  async function handleAnswerUpdate(answerString) {
    setText(answerString);
    await removeAnswer(question);
    await addAnswer(question, answerString);
  }

  return (
    <div className="w-full">
      <Transition.Root className=" space-y-4" show={show}>
        <div className="w-full">
          <div className="w-full pl-2">
            <FadeIn delay="delay-[0ms]">
              <p className="text-2xl sm:text-4xl font-medium">{question.question}</p>
            </FadeIn>
            <FadeIn delay="delay-[100ms]">
              <p className="pt-2 text-gray-600 sm:text-lg">{question.question_instructions}</p>
            </FadeIn>
            <FadeIn delay="delay-[200ms]">
              <div>
                <textarea value={`${text}`} className="my-4 p-4 h-48 w-full border rounded-lg" placeholder="Enter your answer here..." onChange={(e) => handleAnswerUpdate(e.target.value)}></textarea>
              </div>
            </FadeIn>
          </div>
        </div>
      </Transition.Root>
    </div>
  );
}

TextInputQuestion.propTypes = {
  question: PropTypes.object,
  addAnswer: PropTypes.func,
  removeAnswer: PropTypes.func,
  answers: PropTypes.array,
};
