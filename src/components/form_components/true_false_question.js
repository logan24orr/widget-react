/* eslint-disable require-jsdoc */
import React, {useEffect, useState} from 'react';
import {Transition} from '@headlessui/react';
import FadeIn from './fade_in.js';
import PropTypes from 'prop-types';

export default function TrueFalseQuestion({question, addAnswer, removeAnswer, answers}) {
  const [show, setShow] = useState(false);
  const [selectedOption, setSelectedOption] = useState();

  useEffect(() => {
    setShow(true);
    const indexAnswer = answers.findIndex((answer) => answer.id === question.id);
    if (indexAnswer !== -1) {
      setSelectedOption(answers[indexAnswer].answer);
    }
  }, []);

  function handleClick(buttonAnswer) {
    setSelectedOption(buttonAnswer);
    removeAnswer(question);
    addAnswer(question, buttonAnswer);
  }

  return (
    <div className="w-full">
      <Transition.Root className=" space-y-4" show={show}>
        <div className="w-full">
          <div className="pl-2">
            <FadeIn delay="delay-[0ms]">
              <p className="text-2xl sm:text-4xl font-medium">{question.question}</p>
            </FadeIn>
            <FadeIn delay="delay-[100ms]">
              <p className="pt-2 text-gray-600 sm:text-lg">{question.question_instructions}</p>
            </FadeIn>
            <FadeIn delay="delay-[200ms]">
              {question.answer_options.map((option) => (
                <button
                  key={option.value}
                  className={`mt-4 rounded-lg p-4 ${option.value === selectedOption ? 'bg-zinc-200' : ''}`}
                  style={{height: '6rem', width: '100%', border: '1px solid gray'}}
                  onClick={() => handleClick(option.value)}
                >
                  <div className="flex">
                    {option.value === 'yes' &&
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="0.75" stroke="green" className="w-12 h-12" height="48px" style={{width: '48px', height: '48px'}}>
                        <path strokeLinecap="round" strokeLinejoin="round" d="M9 12.75 11.25 15 15 9.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                      </svg>
                    }
                    {option.value === 'no' &&
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="0.75" stroke="red" className="w-12 h-12" height="48px" style={{width: '48px', height: '48px'}}>
                        <path strokeLinecap="round" strokeLinejoin="round" d="m9.75 9.75 4.5 4.5m0-4.5-4.5 4.5M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
                      </svg>
                    }
                    {option.value === 'idk' &&
                      <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="0.75" stroke="orange" className="w-12 h-12" height="48px" style={{width: '48px', height: '48px'}}>
                        <path strokeLinecap="round" strokeLinejoin="round" d="M9.879 7.519c1.171-1.025 3.071-1.025 4.242 0 1.172 1.025 1.172 2.687 0 3.712-.203.179-.43.326-.67.442-.745.361-1.45.999-1.45 1.827v.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Zm-9 5.25h.008v.008H12v-.008Z" />
                      </svg>
                    }
                    <div className="pl-4">
                      <p className="font-bold text-lg text-left">{option.desc}</p>
                      <p className="text-gray-600 text-sm sm:text-lg text-left">{option.sub_desc}</p>
                    </div>
                  </div>
                </button>
              ))}
            </FadeIn>
          </div>
        </div>
      </Transition.Root>
    </div>
  );
}

TrueFalseQuestion.propTypes = {
  question: PropTypes.object,
  addAnswer: PropTypes.func,
  removeAnswer: PropTypes.func,
  answers: PropTypes.array,
};
