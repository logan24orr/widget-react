/* eslint-disable require-jsdoc */
import React from 'react';
import PropTypes from 'prop-types';

export default function FormControls({previousQuestion, nextQuestion, questions, currentQuestionIndex, disabled}) {
  return (
    <div className="w-full flex">
      <div className="w-36 h-full flex justify-center items-center">
        <button className="w-24 mt-4 h-16 flex justify-center items-center bg-gray-200 rounded-lg"
          onClick={previousQuestion}
        >
          <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="2.0" stroke="currentColor" className="w-12 h-12" height="48px" style={{width: '24px', height: '24px'}}>
            <path strokeLinecap="round" strokeLinejoin="round" d="M10.5 19.5 3 12m0 0 7.5-7.5M3 12h18" />
          </svg>
        </button>
      </div>
      <button className={`mt-4 ml-4 w-full p-4 rounded-lg font-semibold ${disabled ? 'bg-gray-200 text-zinc-700' : 'bg-black text-white'} sm:text-lg`}
        onClick={nextQuestion}
        disabled={disabled}
      >
        {currentQuestionIndex === (questions.length - 1) && <p>Complete Submission</p>}
        {currentQuestionIndex < (questions.length - 1) && <p>Next Step</p>}
      </button>
    </div>
  );
}

FormControls.propTypes = {
  previousQuestion: PropTypes.object,
  nextQuestion: PropTypes.object,
  questions: PropTypes.array,
  currentQuestionIndex: PropTypes.number,
  disabled: PropTypes.bool,
};
