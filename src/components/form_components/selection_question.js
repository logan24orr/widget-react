/* eslint-disable require-jsdoc */
import React, {useEffect, useState} from 'react';
import {Transition} from '@headlessui/react';
import FadeIn from './fade_in.js';
import PropTypes from 'prop-types';

export default function SelectionQuestion({question, addAnswer, removeAnswer, answers}) {
  const [show, setShow] = useState(false);
  const [selection, setSelection] = useState('State');

  useEffect(() => {
    setShow(true);
    const indexAnswer = answers.findIndex((answer) => answer.id === question.id);
    if (indexAnswer !== -1) {
      setSelection(answers[indexAnswer].answer);
    }
  }, []);

  function handleClick(answerString) {
    console.log(answerString);
    removeAnswer(question);
    addAnswer(question, answerString);
  }

  return (
    <div className="w-full">
      <Transition.Root className=" space-y-4" show={show}>
        <div className="w-full">
          <div className="w-full pl-2">
            <div className="w-full">
              <FadeIn delay="delay-[0ms]">
                <p className="text-2xl sm:text-4xl font-medium">{question.question}</p>
              </FadeIn>
              <FadeIn delay="delay-[100ms]">
                <p className="pt-2 text-gray-600 sm:text-lg">{question.question_instructions}</p>
              </FadeIn>
              <FadeIn delay="delay-[200ms]">
                <div className="w-full">
                  <label htmlFor="location" className="text-sm font-medium text-gray-900">
                                      Location
                  </label>
                  <select
                    id="location"
                    name="location"
                    className="mt-2 block w-full rounded-md border-0 py-4 px-4  text-gray-900 ring-1 ring-inset ring-gray-300 focus:ring-2 focus:ring-indigo-600 sm:text-sm"
                    defaultValue={`${selection}`}
                    onChange={(e) => handleClick(e.target.value)}
                  >
                    {question.answer_options.map((option) => (
                      <option key={option}>{option}</option>
                    ))}
                  </select>
                </div>
              </FadeIn>
            </div>
          </div>
        </div>
      </Transition.Root>
    </div>
  );
}

SelectionQuestion.propTypes = {
  question: PropTypes.object,
  addAnswer: PropTypes.func,
  removeAnswer: PropTypes.func,
  answers: PropTypes.array,
};
