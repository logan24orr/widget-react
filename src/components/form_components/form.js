/* eslint-disable require-jsdoc */
import React, {useState, useEffect} from 'react';
import {fetchQuestions, submitForm} from '../../api.js';
import TrueFalseQuestion from './true_false_question.js';
import {XMarkIcon} from '@heroicons/react/24/outline';
import cruzFinancingLogo from '../../assets/cruz_financing_logo.svg';
import FormControls from './form_controls.js';
import SelectionQuestion from './selection_question.js';
import TextInputQuestion from './text_input_question.js';
import FormProgress from './form_progress.js';
import ContactInfoQuestion from './contact_info_question.js';
import Finale from './finale.js';
import PropTypes from 'prop-types';

export default function Form({onClose}) {
  const [questions, setQuestions] = useState([]);
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const [answers, setAnswers] = useState([]);
  const [fullName, setFullName] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [showFinale, setShowFinale] = useState(false);
  const [disabled, setDisabled] = useState(true);

  useEffect(() => {
    const fetchFormQuestions = async () => {
      console.log('Fetching form questions');
      const data = await fetchQuestions();
      console.log(data.questions);
      setQuestions(data.questions);
    };

    fetchFormQuestions();
  }, []);

  async function addAnswer(quesion, answerString) {
    setDisabled(false);
    const record = {
      id: quesion.id,
      question_type: quesion.question_type,
      question: quesion.question,
      answer: answerString,
    };
    answers.push(record);
    const items = [...answers];
    setAnswers(items);
  }

  async function removeAnswer(question) {
    const indexToRemove = answers.findIndex((answer) => answer.id === question.id);
    if (indexToRemove !== -1) {
      answers.splice(indexToRemove, 1);
    }
  }

  async function nextQuestion() {
    if (currentQuestionIndex === questions.length - 1) {
      await submitForm(fullName, phone, email, answers);
      setShowFinale(true);
    } else {
      if (currentQuestionIndex < answers.length - 1) {
        setDisabled(false);
      } else {
        setDisabled(true);
      }
      setCurrentQuestionIndex((prevIndex) => prevIndex + 1);
    }
  }

  function previousQuestion() {
    if (currentQuestionIndex >= 1) {
      setDisabled(false);
      setCurrentQuestionIndex((prevIndex) => prevIndex - 1);
    }
  }

  const renderCurrentQuestion = () => {
    const currentQuestion = questions[currentQuestionIndex];

    return (
      <div className="w-full h-full overflow-y-auto">
        {showFinale && <Finale onClose={onClose} />}
        {!showFinale &&
          <div className="w-screen md:w-full h-full flex pt-12 font-sans overflow-y-auto">
            <div className="absolute right-0 top-0 z-50 p-2 block">
              <button
                type="button"
                className="rounded-md bg-white text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-offset-2"
                onClick={onClose}
              >
                <XMarkIcon className="h-6 w-6" aria-hidden="true" />
              </button>
            </div>
            <div key={currentQuestion.id} className="px-8 pb-8 h-full w-full flex justify-center">
              <div className="w-full max-w-2xl">
                <div className="w-full flex items-start">
                  <div className="flex items-center">
                    <img src={cruzFinancingLogo} alt="cruz logo" className="h-14 sm:h-24" style={{marginLeft: '0'}} />
                    <span className="ml-2 sm:ml-4 h-6 sm:h-10 px-2 inline-flex items-center rounded-full bg-green-50 sm:px-4 text-[10px] sm:text-xl font-medium text-green-700 ring-1 ring-inset ring-green-600/20">
                      $500 Cash Advance
                    </span>
                  </div>
                </div>
                {currentQuestion.question_type === 'true_false' && <TrueFalseQuestion question={currentQuestion} addAnswer={addAnswer} removeAnswer={removeAnswer} answers={answers} />}
                {currentQuestion.question_type === 'selection' && <SelectionQuestion question={currentQuestion} addAnswer={addAnswer} removeAnswer={removeAnswer} answers={answers} />}
                {currentQuestion.question_type === 'text_input' && <TextInputQuestion question={currentQuestion} addAnswer={addAnswer} removeAnswer={removeAnswer} answers={answers} />}
                {currentQuestion.question_type === 'contact_info' && <ContactInfoQuestion fullName={fullName} setFullName={setFullName} phone={phone} setPhone={setPhone} email={email} setEmail={setEmail} setDisabled={setDisabled} />}
                <div className="pl-2 sm:pl-0 w-full">
                  <FormControls previousQuestion={previousQuestion} nextQuestion={nextQuestion} questions={questions} currentQuestionIndex={currentQuestionIndex} disabled={disabled} />
                </div>
                <div className="w-full flex justify-center mt-8 pb-24 sm:pb-0">
                  <FormProgress numQuestions={questions.length} questionNumber={currentQuestionIndex} />
                </div>
              </div>
            </div>
          </div >
        }
      </div>
    );
  };

  return (
    <div className="h-full">{questions.length > 0 && renderCurrentQuestion()}</div>
  );
}

Form.propTypes = {
  onClose: PropTypes.func,
};
