/* eslint-disable require-jsdoc */
import React, {useEffect, useState} from 'react';
import {Transition} from '@headlessui/react';
import FadeIn from './fade_in.js';
import PropTypes from 'prop-types';

export default function ContactInfoQuestion({
  fullName,
  setFullName,
  phone,
  setPhone,
  email,
  setEmail,
  setDisabled,
}) {
  const [show, setShow] = useState(false);

  useEffect(() => {
    setShow(true);
  }, []);

  function updateValue(setFunction, value) {
    setFunction(value);
    if (fullName.length > 0 && phone.length > 0 && email.length > 0) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
    setFunction(value);
  }

  return (
    <div className="w-full">
      <Transition.Root className=" space-y-4" show={show}>
        <div className="w-full">
          <div className="w-full pl-2">
            <FadeIn delay="delay-[0ms]">
              <p className="text-2xl sm:text-4xl font-medium">
                Contact Information
              </p>
            </FadeIn>
            <FadeIn delay="delay-[100ms]">
              <p className="pt-2 text-gray-600 text-sm sm:text-lg">
                Please enter your contact information below.
              </p>
            </FadeIn>
            <div className="mt-4">
              <FadeIn delay="delay-[200ms]">
                <div>
                  <label
                    htmlFor="full-name"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Full name
                  </label>
                  <div className="relative mt-2 rounded-md shadow-sm">
                    <input
                      type="text"
                      name="full-name"
                      id="full-name"
                      className="block w-full rounded-md border-0 py-4 pl-4 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      placeholder="Full name"
                      onChange={(e) => updateValue(setFullName, e.target.value)}
                    />
                  </div>
                </div>
              </FadeIn>
              <FadeIn delay="delay-[300ms]">
                <div className="mt-2">
                  <label
                    htmlFor="phone-number"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Phone Number
                  </label>
                  <div className="relative mt-2 rounded-md shadow-sm">
                    <input
                      type="text"
                      name="phone-number"
                      id="phone-number"
                      className="block w-full rounded-md border-0 py-4 pl-4 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      placeholder="(555) 987-6543"
                      onChange={(e) => updateValue(setPhone, e.target.value)}
                    />
                  </div>
                </div>
              </FadeIn>
              <FadeIn delay="delay-[400ms]">
                <div className="mt-2">
                  <label
                    htmlFor="email"
                    className="block text-sm font-medium leading-6 text-gray-900"
                  >
                    Email
                  </label>
                  <div className="relative mt-2 rounded-md shadow-sm">
                    <input
                      type="email"
                      name="email"
                      id="email"
                      className="block w-full rounded-md border-0 py-4 pl-4 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
                      placeholder="you@example.com"
                      onChange={(e) => updateValue(setEmail, e.target.value)}
                    />
                  </div>
                </div>
              </FadeIn>
              <FadeIn delay="delay-[500ms]">
                <div className="mt-2">
                  <div className="relative flex items-start">
                    <div className="flex h-6 items-center">
                      <input
                        id="candidates"
                        aria-describedby="candidates-description"
                        name="candidates"
                        type="checkbox"
                        className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-600"
                      />
                    </div>
                    <div className="ml-3 text-sm leading-6">
                      <label
                        htmlFor="candidates"
                        className="font-medium text-gray-900"
                      >
                        I agree to
                      </label>{' '}
                      <span id="candidates-description" className="text-black">
                        {/* eslint-disable-next-line react/no-unescaped-entities */}
                        <span className="underline text-black">
                          {' '}
                          <a href="https://lendform-legal-docs.s3.us-west-2.amazonaws.com/cruz_terms_of_use.html">
                            Cruz Financing&aposs Terms of Use
                          </a>
                        </span>{' '}
                        and to be Contacted by Lowe Law Group
                      </span>
                    </div>
                  </div>
                </div>
              </FadeIn>
            </div>
          </div>
        </div>
      </Transition.Root>
    </div>
  );
}

ContactInfoQuestion.propTypes = {
  fullName: PropTypes.string,
  setFullName: PropTypes.func,
  phone: PropTypes.string,
  setPhone: PropTypes.func,
  email: PropTypes.string,
  setEmail: PropTypes.func,
  setDisabled: PropTypes.func,
};
