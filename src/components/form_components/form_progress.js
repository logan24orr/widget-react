/* eslint-disable require-jsdoc */
import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';

export default function FormProgress({numQuestions, questionNumber}) {
  const [steps, setSteps] = useState([]);

  useEffect(() => {
    function calculateStatus(index) {
      if (index === questionNumber) {
        return 'current';
      } else if (index < questionNumber) {
        return 'complete';
      } else if (index > questionNumber) {
        return 'upcoming';
      }
    }

    const steps = [];
    for (let i = 0; i < numQuestions; ++i) {
      steps.push({name: `Question ${i + 1}`, href: '#', status: `${calculateStatus(i)}`});
    }
    setSteps(steps);
  }, []);

  return (
    <nav className="flex items-center justify-center" aria-label="Progress">
      <ol role="list" className="ml-8 flex items-center space-x-5">
        {steps.map((step) => (
          <li key={step.name}>
            {step.status === 'complete' ?
              (
              <a href={step.href} className="block h-2.5 w-2.5 rounded-full bg-black">
                <span className="sr-only">{step.name}</span>
              </a>
                ) :
              step.status === 'current' ?
                (
              <a href={step.href} className="relative flex items-center justify-center" aria-current="step">
                <span className="absolute flex h-5 w-5 p-px" aria-hidden="true">
                  <span className="h-full w-full rounded-full bg-gray-300" />
                </span>
                <span className="relative block h-2.5 w-2.5 rounded-full bg-black" aria-hidden="true" />
                <span className="sr-only">{step.name}</span>
              </a>
                  ) :
                (
              <a href={step.href} className="block h-2.5 w-2.5 rounded-full bg-gray-200 hover:bg-gray-400">
                <span className="sr-only">{step.name}</span>
              </a>
                  )}
          </li>
        ))}
      </ol>
    </nav>
  );
}

FormProgress.propTypes = {
  numQuestions: PropTypes.number,
  questionNumber: PropTypes.number,
};
