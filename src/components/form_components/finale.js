/* eslint-disable require-jsdoc */
import React, {useEffect, useState} from 'react';
import {Transition} from '@headlessui/react';
import FadeIn from './fade_in.js';
import {XMarkIcon} from '@heroicons/react/24/outline';
import cruzFinancingLogo from '../../assets/cruz_financing_logo_white.svg';
import PropTypes from 'prop-types';

export default function Finale({onClose}) {
  const [show, setShow] = useState(false);

  useEffect(() => {
    setShow(true);
  }, []);

  return (
    <div className="w-full h-full min-h-[750px] relative flex justify-center items-center bg-black">
      <Transition.Root className="space-y-4" show={show}>
        <div className="absolute right-0 top-0 p-2">
          <button
            type="button"
            className="rounded-md bg-transparent text-gray-100 focus:outline-none  focus:ring-offset-2"
            onClick={onClose}
          >
            <span className="sr-only">Close</span>
            <XMarkIcon className="h-6 w-6" aria-hidden="true" />
          </button>
        </div>
        <div className="h-full flex flex-col justify-center">
          <div className="flex flex-col justify-center w-full h-full p-8">
            <FadeIn delay="delay-[0ms]">
              <div className="flex justify-center mb-8">
                <img
                  src="https://versity-images.s3.us-west-2.amazonaws.com/Success.svg"
                  alt="congrats"
                  width="200px"
                  height="100%"
                />
              </div>
            </FadeIn>
            <FadeIn delay="delay-[50ms]">
              <div className="flex justify-center mb-4">
                <p className="font-extrabold md:text-5xl text-2xl tracking-tighter text-white">THANK YOU</p>
              </div>
            </FadeIn>
            <FadeIn delay="delay-[100ms]">
              <div className="flex justify-center w-full">
                <p className="font-medium text-center p-4 text-lg w-96 text-white">We will be in touch within 24 hours with more information about this $500 offer </p>
              </div>
            </FadeIn>
            <FadeIn delay="delay-[100ms]">
              <div className="flex justify-center w-full">
                <p className="font-medium text-center p-4 text-lg w-96 text-white">For instant assistance, call Lowe Law Group now at 801-900-4681</p>
              </div>
            </FadeIn>
          </div>
          <div className="w-full flex justify-center fixed bottom-0 left-0 mb-1">
            <FadeIn delay="delay-[0ms]">
              <div className="mt-4 mb-2">
                <div className="w-full flex justify-center absolute bottom-0 left-0 mb-1">
                  <img src={cruzFinancingLogo} alt="cruz logo" className="h-12 w-auto inline" />
                </div>
              </div>
            </FadeIn>
          </div>
        </div>
      </Transition.Root>
    </div>
  );
}

Finale.propTypes = {
  onClose: PropTypes.func,
};
