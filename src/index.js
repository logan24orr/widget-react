import React from 'react';
import {createRoot} from 'react-dom/client';
import App from './app.js';

const lendformContainer = document.createElement('div');
lendformContainer.id = 'lendformContainer';
lendformContainer.className = 'z-[2147482998]';

document.body.appendChild(lendformContainer);

const root = createRoot(document.getElementById('lendformContainer'));
root.render(<App />);
