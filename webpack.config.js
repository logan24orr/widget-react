import path from 'path';
import Dotenv from 'dotenv-webpack';

const config = {
  mode: 'development', // or 'production' or 'none'
  entry: './src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve('./dist'),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader', 'postcss-loader'],
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192, // Convert images < 8kb to base64 strings
              name: 'images/[name].[hash:8].[ext]', // Rename images if needed
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new Dotenv(),
  ],
};

export default config;
