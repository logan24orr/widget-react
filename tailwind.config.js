/** @type {import('tailwindcss').Config} */
// tailwind.config.js

import colors from 'tailwindcss/colors';

module.exports = {
  content: [
    // './index.html',
    './src/**/*.js',
    './src/**/*.css',
    // Add any additional files you want Tailwind to scan for classes
  ],
  theme: {
    extend: {},
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      emerald: colors.emerald,
      green: colors.green,
      indigo: colors.indigo,
      yellow: colors.yellow,
      red: colors.red,
      zinc: colors.zinc,
    },
  },
  plugins: [],
};
